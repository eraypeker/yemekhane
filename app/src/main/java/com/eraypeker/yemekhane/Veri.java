package com.eraypeker.yemekhane;

public class Veri {
    private String date;
    private String title;
    private String link;

    public String getDate() {
        return this.date;
    }

    public String getTitle() {
        return this.title;
    }

    public String getLink() {
        return this.link;
    }

    public Veri(String date, String title, String link) {
        this.date = date;
        this.title = title;
        this.link = link;
    }
    public Veri() {
    }
}
