package com.eraypeker.yemekhane;

import android.content.Context;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import java.io.File;
/*import android.view.LayoutInflater;
import android.view.Menu;
import android.widget.FrameLayout;
import android.widget.SearchView;
*/

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView courseRV;
    boolean a;

    // Arraylist for storing data
    private ArrayList<Veri> veriArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        a = true;

        courseRV = findViewById(R.id.idRVCourse);

        veriArrayList = new ArrayList<>();
        new doIT().execute();
        //System.out.println(veriArrayList.size());
        CourseAdapter courseAdapter = new CourseAdapter(this, veriArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        courseRV.setLayoutManager(linearLayoutManager);
        courseRV.setAdapter(courseAdapter);
        while (a){
            courseAdapter.notifyDataSetChanged();
        }
        //System.out.println("iea");
        //System.out.println(veriArrayList.size());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            trimCache(this);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }


    public class doIT extends AsyncTask<String,String,String> {
        String url;
        @Override
        protected String doInBackground(String... params) {
            try {
                url = "http://www.sksdb.hacettepe.edu.tr/YemekListesi.xml";
                Document document = Jsoup.connect(url).get();

                Elements dates = document.getElementsByTag("gun");
                //System.out.println("d");
                for (Element date : dates){
                    String yemek = "";
                    Integer count = 1;

                    Elements foods = date.select("yemek");
                    foods.size();
                    for (Element food : foods){
                        if (count != foods.size())
                            yemek = yemek + food.text() + "\n";
                        else
                            yemek = yemek + food.text();
                        count += 1;
                    }

                    veriArrayList.add(new Veri(date.select("tarih").text().split(" <")[0] + "\n", yemek, "\nKalori: " + date.select("kalori").text()));
                }
                a = false;
            } catch (IOException e) {
                e.printStackTrace();
            } return null;
        }

        /*@Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            System.out.println(veriArrayList.size());
            //tv1.setText(dosya);
        }*/

    }
}