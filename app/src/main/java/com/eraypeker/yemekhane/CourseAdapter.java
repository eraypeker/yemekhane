package com.eraypeker.yemekhane;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.Viewholder> {

    private Context context;
    private ArrayList<Veri> VeriArrayList;

    // Constructor
    public CourseAdapter(Context context, ArrayList<Veri> VeriArrayList) {
        this.context = context;
        this.VeriArrayList = VeriArrayList;
    }

    @NonNull
    @Override
    public CourseAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // to inflate the layout for each item of recycler view.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CourseAdapter.Viewholder holder, int position) {
        // to set data to textview and imageview of each card layout
        Veri model = VeriArrayList.get(position);
        holder.dateTv.setText(model.getDate());
        holder.titleTv.setText(model.getTitle());
        holder.urlTv.setText(model.getLink());

        /*

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            private Object MainActivity;



            @Override
            public void onClick(View view) {
                System.out.println("tıkk");
                Intent intent = new Intent(view.getContext(), IntentActivity.class);
                String link = model.getLink();
                intent.putExtra("link", link);
                view.getContext().startActivity(intent);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        // this method is used for showing number
        // of card items in recycler view.
        return VeriArrayList.size();
    }

    // View holder class for initializing of
    // your views such as TextView and Imageview.
    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView dateTv, titleTv, urlTv;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            dateTv = itemView.findViewById(R.id.dateTv);
            titleTv = itemView.findViewById(R.id.titleTv);
            urlTv = itemView.findViewById(R.id.urlTv);
        }
    }
}
